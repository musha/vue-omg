import $ from 'jquery'

var currentUrl = '//gongyi3act.tencent-cloud.net', // 当前域名 默认为正式地址
    canshare = 1; // 是否可以分享 默认可以
if(location.hostname.indexOf('gongyi3act') < 0) {
    // 不是正式域名
    if(location.hostname.indexOf('gongyi3test') >= 0) {
        // 是测试域名
        currentUrl = '//gongyi3test.tencent-cloud.net';
    } else {
        // 是本地测试 不调接口
        canshare = 0;
    };
};

var wxConfig = {
    debug: false, 
    jsApiList: [ 'checkJsApi', 'onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo']
};

function wxshare() {
    $.ajax({
        type: 'GET',
        url: currentUrl + '/cgi-bin/wx_privilege',
        dataType: 'json',
        data: {
            spid: 'SZ00000012',//SZ00000012
            cmd: 'page',
            url: location.href.split('#')[0]
        },
        timeout: 300,
        success: function (data) {
            wxConfig.appId = data.appid;
            wxConfig.timestamp = data.timestamp;
            wxConfig.nonceStr = data.noncestr;
            wxConfig.signature = data.signature;
            wx.config(wxConfig);
            wx.ready(function(){
                var title = '一个“噢买瓜”的故事';
                var desc = '比OhMyGod更带货的是噢买瓜';
                var link = window.location.protocol + currentUrl + '/omg20/';
                var imgUrl = window.location.protocol + currentUrl + '/omg20/share.jpg';
                wx.onMenuShareTimeline({
                    title: title,
                    link: link,
                    imgUrl: imgUrl,
                    success: function () {},
                    cancel: function () {}
                });

                wx.onMenuShareAppMessage({
                    title: title,
                    desc: desc,
                    link: link,
                    imgUrl: imgUrl,
                    type: '', // 分享类型,music、video或link，不填默认为link
                    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                    success: function () {},
                    cancel: function () {}
                });
            });
            wx.error(function(res){
                console.log(res);
            });
        },
        error: function (xhr, type) {
            console.log('jssdk error!');
        }
    });
};
setTimeout(() => {
    if(canshare == 1) {
        wxshare();
    };
}, 1000);


