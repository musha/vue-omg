import Vue from 'vue'
import VueRouter from 'vue-router'
import loading from '@/views/loading.vue'
import home from '@/views/home.vue'
import intro from '@/views/intro.vue'
import choose from '@/views/choose.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'loading',
        component: loading
    },
    {
        path: '/home',
        name: 'home',
        component: home
    },
    {
        path: '/intro',
        name: 'intro',
        component: intro
    },
    {
        path: '/choose',
        name: 'choose',
        component: choose
    }
    // {
    //     path: '/about',
    //     name: 'About',
    //     // route level code-splitting
    //     // this generates a separate chunk (about.[hash].js) for this route
    //     // which is lazy-loaded when the route is visited.
    //     component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    // }
]

const router = new VueRouter({
    routes
})

export default router
