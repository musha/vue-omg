import Vue from 'vue'
import App from './App.vue'
import router from './router'

// vendor
import ydui from '../public/vendor/ydui.flexible.js'; // 页面分辨率
import MtaH5 from 'mta-h5-analysis'; // mta
window.MtaH5 = MtaH5;

// import Vconsole from 'vconsole'; // vconsole
// const vConsole = new Vconsole();
// export default vConsole;

Vue.config.productionTip = false

// 初始化
MtaH5.init({
	"name": 'omg',
  	"sid": '500732124', //必填，统计用的appid
  	"cid": '500732145', //如果开启自定义事件，此项目为必填，否则不填
  	"autoReport": 0,//是否开启自动上报(1:init完成则上报一次,0:使用pgv方法才上报)
  	"senseHash": 1, //hash锚点是否进入url统计
  	"senseQuery": 1, //url参数是否进入url统计
  	"performanceMonitor": 0,//是否开启性能监控
  	"ignoreParams": [] //开启url参数上报时，可忽略部分参数拼接上报
});

new Vue({
  	router,
  	render: h => h(App)
}).$mount('#app')
