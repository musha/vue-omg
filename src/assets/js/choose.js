// console.log('page choose');

import swiper from '../../../public/vendor/swiper/swiper.min.js';
import '../../../public/vendor/swiper/swiper.min.css';
export default {
	data() {
		return {
			thankShow: false,
			donateShow: false,
			posterShow: false,
			index: 0,
			posterIndex: 0
		};
	},
	mounted() {
		MtaH5.pgv();
		let that = this;
		this.$nextTick(() => {
			setTimeout(() => {
				$('.pagin').offset({top: that.$refs.pic.height * 0.63});
			}, 800);
			this.farmerSwiper =  new Swiper('.box-swiper .farmers', {
				initialSlide: that.index,
				direction : 'vertical',
				onSlideChangeEnd: function(swiper) {
					// console.log(swiper.realIndex);
					that.index = swiper.realIndex;
				}
			});
		});
	},
	methods: {
		toPrev() {
			this.farmerSwiper.slidePrev();
		},
		toNext() {
			this.farmerSwiper.slideNext();
		},
		// thank
		toThank() {
			this.thankShow = true;
		},
		// close thank
		closeModalThank() {
			this.thankShow = false;
		},
		// donate
		modalDonate() {
			MtaH5.clickStat('donate');
			this.donateShow = true;
		},
		// to tx
		toTX() {
			MtaH5.clickStat('tx');
		},
		// close donate
		closeModalDonate() {
			this.donateShow = false;
		},
		// 涨粉
		modalPoster(n) {
			switch(n) {
				case 0:
					MtaH5.clickStat('farmer1');
					break;
				case 1:
					MtaH5.clickStat('farmer2');
					break;
				case 2:
					MtaH5.clickStat('farmer3');
					break;
				default:
					//
			};
			this.posterIndex = n;
			this.posterShow = true;
		},
		// close donate
		closeModalPoster() {
			this.posterShow = false;
		}
	}
};
