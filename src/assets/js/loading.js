// console.log('page loading');

import jQuery from '../../../public/vendor/loader/jquery-1.8.3.min.js' // jquery
import html5Loader from '../../../public/vendor/loader/jquery.html5Loader.min.js' // html5loader
export default {
	data() {
		return {
			source: [
                // {src: require('../img/common/bg.jpg')}
            ]
		};
	},
	mounted() {
        MtaH5.pgv();

        let that = this,
		// 预加载
            firstLoadFiles = {
            'files': [
                // common
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/common/arrowl.png'),
                    'size': 759
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/common/arrowr.png'),
                    'size': 747
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/common/bg.jpg'),
                    'size': 219136
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/common/fruit1.png'),
                    'size': 13312
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/common/fruit2.png'),
                    'size': 8192
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/common/fruit3.png'),
                    'size': 16384
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/common/logo.png'),
                    'size': 19456
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/common/tip.png'),
                    'size': 7168
                },
                // home
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/home/play.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/home/text.png'),
                    'size': 22528
                },
                // intro
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/intro/text.png'),
                    'size': 38912
                },
                // choose
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/btn-donate.png'),
                    'size': 6144
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/btn1.png'),
                    'size': 5120
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/btn2.png'),
                    'size': 4096
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/close.png'),
                    'size': 507
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/farmer1.jpg'),
                    'size': 264192
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/farmer2.jpg'),
                    'size': 253952
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/farmer3.jpg'),
                    'size': 244736
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/list.png'),
                    'size': 11264
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/poster1.jpg'),
                    'size': 378880
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/poster2.jpg'),
                    'size': 375808
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/poster3.jpg'),
                    'size': 338944
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/preview1.jpg'),
                    'size': 95232
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/preview2.jpg'),
                    'size': 97280
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/preview3.jpg'),
                    'size': 88064
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/text.png'),
                    'size': 10240
                },
                {
                    'type': 'IMAGE',
                    'source': require('@/assets/img/choose/tip-p.png'),
                    'size': 2048
                }
            ]
        };

		$.html5Loader({
            filesToLoad: firstLoadFiles,
            onBeforeLoad: function() {},
            onElementLoaded: function(obj, elm) {},
            onUpdate: function(percentage) {
                // console.log(percentage);
                $('.page-loading .bar').animate({
                    height: (percentage + '%')},
                    30, function() {
                    /* stuff to do after animation is complete */
                });
            },
            onComplete: function() {
                setTimeout(() => {
                    that.$router.replace('/home');
                }, 1000);
            }
        });
	},
	methods: {
		// 
	}
};