// console.log('page home');

export default {
	data() {
		return {
			browserw: window.innerWidth|| document.documentElement.clientWidth || document.body.clientWidth,
			browserh: window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
			browserwh: '',      // 浏览器宽高比
			playerwh: 1.77778, // 视频宽高比
			txplayer: '',         // 腾讯视频
			show: false
		};
	},
	mounted() {
		MtaH5.pgv();

		let that = this;
		// 浏览器宽高比
		that.browserwh = that.browserh/that.browserw;
		that.$nextTick(() => {
			that.txplayer = new Txplayer({
			  	containerId: 'player',
			  	vid: 'h3200n3f55m',
			  	isUsePreload: true,
			  	showRecommendOnEnd: false,
			});

			let getPlayer = setInterval(() => {
				let len = document.getElementsByClassName('txp_player').length;
				if(len > 0) {
					let player = document.getElementsByClassName('txp_player');
					if(that.browserwh > that.playerwh) {
						// 左右居中 视频高度等于浏览器宽度
						player[0].style.width = that.browserw * that.playerwh + 'px';
						player[0].style.height = that.browserw + 'px';
					} else {
						// 上下居中 视频宽度等于浏览器高度
						player[0].style.width = that.browserh + 'px';
						player[0].style.height = that.browserh / that.playerwh + 'px';
					};
					clearInterval(getPlayer);
					// 去掉控件
					let getControl = setInterval(() => {
						let conlen = document.getElementsByClassName('txp_btn_fullscreen').length;
						if(len > 0) {
							let control = document.getElementsByClassName('txp_btn_fullscreen');
							if(control[0] != undefined || control[0] != null) {
								control[0].style.display = 'none';
								clearInterval(getControl);
							};
						};
					}, 30);
					let getControl2 = setInterval(() => {
						let conlen = document.getElementsByClassName('txp_btn_scrubber').length;
						if(len > 0) {
							let control = document.getElementsByClassName('txp_btn_scrubber');
							if(control[0] != undefined || control[0] != null) {
								control[0].style.opacity = '0';
								clearInterval(getControl2);
							};
						};
					}, 30);
					// skip
					let btnT = (that.browserh + parseFloat(player[0].style.width))/2;
					$('.btn-skip').css({top: btnT * 0.9});
					if(parseFloat(player[0].style.height) < that.browserw) {
						let btnR = (that.browserw - parseFloat(player[0].style.height))/2;
						$('.btn-skip').css({right: btnR * 0.72});
					};
				};
			}, 100);
		});

		// this.browserwh = this.browserh/this.browserw;
		// let that = this;
		// let ad = this.$refs.ad,
		// // 获取视频的实际宽高比例
		// getPlayerWH = setInterval(() => {
		// 	if(this.playerw == 0) {
		// 		if(ad.videoWidth == undefined || ad.videoWidth == null) {
		// 			// 不存在videoWidth
		// 			clearInterval(getPlayerWH);
		// 		} else {
		// 			// 存在videoWidth
		// 			if(ad.videoWidth != 0) {
		// 				this.playerw = ad.videoWidth;
		// 				this.playerh = ad.videoHeight;
		// 			};
		// 		};
		// 	} else {
		// 		let playerwh = this.playerw/this.playerh;
		// 		if(this.browserwh < playerwh) {
		// 			// 全屏幕高度为准
		// 			ad.style.width = this.browserwh * 100 + '%';
		// 		};
		// 		clearInterval(getPlayerWH);
		// 	};
		// }, 100);
	},
	methods: {
		// play
        toPlay() {
        	MtaH5.clickStat('player');
            this.show = true;
            this.txplayer.play();
            setTimeout(() => {
            	$('.btn-skip').fadeIn('fast');
            }, 10000);
			this.txplayer.on('playStateChange', (state) => {
				if(state == 0) {
					this.$router.replace('/intro');
				}
			});
        },
        // skip
        skip() {
        	// this.txplayer.pause();
        	this.$router.replace('/intro');
        }
	}
};
